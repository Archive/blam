//
// Author:
//   Mario Chavez <>
//
// (C) 2004 Mario Chavez
//

using GConf;
using System;
using System.Net;

namespace Imendio.Blam {
    public class Proxy {
	private static string PROXY_BASE =      "/system/http_proxy";
	private static string USE_PROXY = PROXY_BASE + "/use_http_proxy";
	private static string HOST =      PROXY_BASE + "/host";
	private static string PORT =      PROXY_BASE + "/port";
	private static string USER =      PROXY_BASE + "/authentication_user";
	private static string PASSWORD =  PROXY_BASE + "/authentication_password";

        public static bool   UseProxy;
        public static string ProxyHost;
        public static int    ProxyPort;

        public static event BlamEventHandler Updated;

	private static WebProxy proxy = null;

        public Proxy() {}

	public static WebProxy GetProxy()
	{
	    return proxy;
	}

	public static void InitProxy ()
	{
	    RereadProxySettings ();
	    
	    Conf.AddNotify (PROXY_BASE,
			    new NotifyEventHandler (ConfNotifyHandler));
	}

	private static void RereadProxySettings () 
	{
            bool updated = false;

            try {
                bool use_proxy;

                use_proxy = Conf.Get (USE_PROXY, false);
		if (use_proxy) {
                    if (UseProxy == false) {
                        updated = true;
                    }

                    UseProxy = true;

                    string host;
		    int    port;
		    string proxyUrlString;

		    host = Conf.Get (HOST, "");
                    port = Conf.Get (PORT, 0);

                    if (host != ProxyHost || port != ProxyPort) {
                        updated = true;
                    }
                    ProxyHost = host;
                    ProxyPort = port;

		    proxyUrlString = "http://" + host + ":" + port.ToString();

		    proxy = new WebProxy();
                    //Console.Error.WriteLine("Using proxy " + proxyUrlString);

                    Uri proxyUrl = new Uri (proxyUrlString);
                    proxy.Address = proxyUrl;

                    string username = Conf.Get (USER, "");
		    string password = Conf.Get (PASSWORD, "");
                    if (username != null) {
                       proxy.Credentials = new NetworkCredential (username, password);
		    }
		} else {
                    if (UseProxy == true) {
                        updated = true;
                    }
                    UseProxy = false;
		    proxy = null;
		}
	    } catch(Exception) {
		proxy = null;
	    }

            if (updated == true && Updated != null) {
                Updated ();
            }
	}

	private static void ConfNotifyHandler (object sender,
					       NotifyEventArgs args)
	{
	    Console.Error.WriteLine ("Rereading the proxy settings\n");
	    RereadProxySettings ();
	}
    }
}
