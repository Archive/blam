// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio AB
// 

using System.Collections;
using System;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ServiceModel.Syndication;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.CompilerServices;

namespace Imendio.Blam {

    public interface IChannel
    {
        int NrOfItems {get; }
        int NrOfUnreadItems {get; }
        string Name {get; set; }
        string Url {get; set; }
        ArrayList ItemList {get; set; }
        Gtk.TreeIter Iter {get; set; }
        bool MarkAsRead();
        Item GetItem(string id);
        void Setup();
        event PropertyChangedEventHandler PropertyChanged;
        void RemoveItems();
		Task<bool> RefreshAsync();
    }

    public class Channel : IChannel, INotifyPropertyChanged {
		// Used when updating the feed
		[XmlAttribute] public string LastModified = "";
		[XmlAttribute] public string ETag = "";
		[XmlAttribute] public string Type = "";
		[XmlAttribute] public string Keywords = "";
		[XmlAttribute] public DateTime LastRefreshed = new DateTime (0);
		
		// HTTP authentication options
		[XmlAttribute] public string http_username = "";
		[XmlAttribute] public string http_password = "";

        ArrayList item_list = null;
        Object obj = new Object();

		public event PropertyChangedEventHandler PropertyChanged;

		string name;
		[XmlAttribute]
		public string Name {
			get {
				return name;
			}
			set {
				if (name != value) {
					name = value;
					NotifyPropertyChanged();
				}
			}
		}

		string url;
		[XmlAttribute]
		public string Url {
			get {
				return url;
			}
			set {
				if (url != value) {
					url = value;
					NotifyPropertyChanged();
				}
			}
		}

        private Gtk.TreeIter mIter;

		public int NrOfItems {
			get {
                lock(obj){
                    return item_list.Count;
                }
			}
		}

        public int NrOfUnreadItems {
            get {
                int unread = 0;

                Item item;
                lock(obj){
                    foreach (string id in item_list) {
                        if(id != null){
                            item = ItemStore.Get(id);
                            if(item.Unread){
                                ++unread;
                            }
                        }
                    }
                }

                return unread;
            }
        }

        public int NrOfNewItems {
            get {
                int new_items = 0;
                Item item;
                lock(obj){
                    foreach(string id in item_list){
                        if(id == null){ /* TODO: Figure out why this happens */
                            continue;
                        }
                        item = ItemStore.Get(id);
                        if(item != null && item.Unread && !item.Old){
                            ++new_items;
                        }
                    }
                }

                return new_items;
            }
        }

        public ArrayList ItemList {
            get {
                lock(obj){
                    return item_list;
                }
            }
            set {
                lock(obj){
                    item_list = value;
                    Setup();
                }
            }
        }

		void NotifyPropertyChanged([CallerMemberName] string name = "")
		{
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}

		[XmlIgnore]
        public Gtk.TreeIter Iter {
            get {
                return mIter;
            }
            set {
                mIter = value;
            }
        }

        public Channel ()
        {
            item_list = new ArrayList();
            mIter = new Gtk.TreeIter();
        }

        public Channel (string name, string url) : this()
        {
            Name = name;
            Url = url;
        }

        public Item GetItem (string id)
        {
            return ItemStore.Get(id);
        }

        public void Setup()
        {
            lock(obj){
                ArrayList nlist = new ArrayList();
                foreach(string id in item_list){
                    if(id == null){
                        continue;
                    }
                    Item item = ItemStore.Get(id);
                    if(item == null){
                    } else {
                        nlist.Add(id);
                        item.PropertyChanged += ItemChanged;
                    }
                }
                item_list = nlist;
            }
        }

		public void RemoveItems()
		{
			lock(obj){
				foreach(string id in item_list){
					ItemStore.Remove(id);
				}
			}
		}

		public bool MarkAsRead ()
		{
			bool updated = false;
			Item item;

			lock (obj) {
				foreach (string id in item_list) {
					if (id == null) {
						System.Console.WriteLine("null id {0} on {1}", id, Url);
						continue;
					}
					item = ItemStore.Get(id);
					if (item.Unread) {
						item.Unread = false;
						updated = true;
					}
				}
			}

			if (updated)
				NotifyPropertyChanged("NrOfUnreadItems");

			return updated;
		}

        /**
         * Update this channel with the given feed
         */
		public bool Update(SyndicationFeed feed)
		{
			/* If our name is sub-optimal */
			if((Name == "" || Name == Url) &&
			   feed.Title.Text != null) {
				Name = HtmlUtils.StripHtml(feed.Title.Text.Trim());
			}

			LastRefreshed = DateTime.Now;

			lock (obj ){
				var common = feed.Items.Where(i => item_list.Contains(i.Id));
				foreach (var item in common)
					ItemStore.Get(item.Id).Update(item);

				// new items
				foreach (var item in feed.Items.Except(common))
					ItemStore.Add(new Item(item));

				// items no longer in the feed are evicted
				var ids = feed.Items.Select(i => i.Id);
				var evict = item_list.Cast<string>().Except(ids);
				foreach (var id in evict)
					ItemStore.Remove(id);

				item_list = new ArrayList(ids.ToArray());
			}

			NotifyPropertyChanged("NrOfUnreadItems");
			return true;
		}

		void ItemChanged(object sender, PropertyChangedEventArgs args)
		{
			switch (args.PropertyName) {
				case "Unread":
					NotifyPropertyChanged("NrOfUnreadItems");
					break;
				default:
					throw new InvalidOperationException("Item changed in unexpected way. Shouldn't happen");
					break;
			}
		}

        /**
         * Adds or updates the entry
         */
        public void Add(Item item)
        {
            if(item_list.Contains(item.Id)){
                /* In this case we only need to update */
                ItemStore.Get(item.Id).Update(item);
                return;
            }

			ItemStore.Add(item);
            lock(obj){
                item_list.Add(item.Id);
            }
			ItemStore.Get(item.Id).PropertyChanged += ItemChanged;
        }

		public bool GetHasKeyword (string keyword)
		{
			if (Keywords.IndexOf (keyword) >= 0) {
				return true;
			}

			return false;
		}

		bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			return Conf.Get(Preference.IGNORE_SSL_ERR, false);
		}

		SyndicationFeed LoadFeed(string url)
		{
			var opts = new XmlReaderSettings();
			opts.CheckCharacters = false;
			var reader = XmlReader.Create(url, opts);
			return SyndicationFeed.Load(reader);
		}

		public async Task<bool> RefreshAsync()
		{
			ServicePointManager.ServerCertificateValidationCallback = ValidateCertificate;
			try {
				var feed = await Task.Run(() => LoadFeed(Url));

				return Update(feed);
			} catch (Exception e) {
				Console.WriteLine("Failed to get feed {0}: {1}", Name, e.Message);
				return false;
			}
		}
	}
}
