//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio AB
// (C) 2008 Nuanti Ltd.
//

using GConf;
using Gtk;
using System;
using GtkSharp;
using WebKit;
using Mono.Unix;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using System.Net;
using System.Text;
using System.ComponentModel;
using System.ServiceModel.Syndication;

namespace Imendio.Blam {
    public class ItemView : Gtk.EventBox {
        private WebView webView;
#if ENABLE_FONTS
        private WebSettings webSettings;
#endif
    private string baseDir = null;
	private string last_link = null;
	public bool PageLoaded;

        public event StringUpdatedHandler OnUrl;

		Item _Current;
		public Item Current {
			get {
				return _Current;
			}
			set {
				if (_Current != value) {
					_Current = value;
					Load();
				}
			}
		}

		ItemList itemList;

        public WebView Widget {
            get {
                return webView;
            }
        }

		public ItemView(ItemList list) : base()
		{
            itemList = list;
			itemList.PropertyChanged += ItemListPropertyChanged;
            this.webView = new WebView ();
#if ENABLE_FONTS
            this.webSettings = new WebSettings ();
            webView.Settings = webSettings;
#endif
						ScrolledWindow sw = new ScrolledWindow ();
						sw.Add(webView);
            Add(sw);
	   
            Conf.AddNotify (Preference.FONT_PATH,
                                        new NotifyEventHandler (FontNotifyHandler));
            SetFonts ();

            ProxyUpdatedCb ();
            Proxy.Updated += ProxyUpdatedCb;

		webView.NavigationRequested += delegate (object sender, NavigationRequestedArgs args) {
        try {
			/*
			 * If last_link is the same as args.Request.Uri, the user clicked on a link
			 * (as we know he was hovering). Thus, try to open it on the browser
			 */
			if(args.Request.Uri.Equals(last_link)){
				args.RetVal = NavigationResponse.Ignore;
				GtkBeans.Global.ShowUri(null, args.Request.Uri);
			} else {
				/* Otherwise, it's part of the post, so accept it (like the facebook iframe) */
				args.RetVal = NavigationResponse.Accept;
			}
        }
        catch (Exception e) {
            Console.Error.WriteLine("Couldn't show URL: " + args.Request.Uri + e.Message);
        }
		};

            webView.HoveringOverLink += delegate (object sender, HoveringOverLinkArgs args) {
                if (OnUrl != null)
                  OnUrl (args.Link);
				  last_link = args.Link;
            };

            webView.Show ();
            PageLoaded = false;
        }

		void ItemListPropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName.Equals("Current"))
				Current = ((ItemList)sender).Current;
		}

        private void Load()
        {
			Theme theme = Application.TheApp.ThemeManager.CurrentTheme;
            string author;
            if(Current.Authors.Count == 0){
                author = string.Empty;
            } else if(Current.Authors[0].Name != null){
                author = String.Format(Catalog.GetString("by {0}"), Current.Authors[0].Name);
            } else {
                author  = "&nbsp;";
            }
            string link    = Catalog.GetString("Show in browser");
            string pubdate = (!Current.LastUpdatedTime.Equals(DateTimeOffset.MinValue)) ?
			                    Current.LastUpdatedTime.ToString("D", System.Globalization.CultureInfo.CurrentUICulture) :
				(!Current.PublishDate.Equals(DateTimeOffset.MinValue)) ?
                  Current.PublishDate.ToString("D", System.Globalization.CultureInfo.CurrentUICulture) :
				  Catalog.GetString("[No date available]");
            string text = Current.Summary != null ? Current.Summary.Text
                : Current.Content != null ? (Current.Content as TextSyndicationContent).Text : null;
			string title = Current.Title.Text;
            string url = Current.Links.Count == 0 ? Current.Id :
                Current.Links[0].Uri.ToString();

            baseDir        = "file://" + theme.Path;

            var replaces = new Dictionary<string, string> {
                {"author", author},
                {"link", link},
                {"pubdate", pubdate},
                {"text", text},
                {"title", title},
                {"url", url},
                {"localbase", baseDir},
            };

			webView.LoadString(theme.Render(replaces), null, null, baseDir);
	}

        private void SetFonts ()
        {
            string varFont = Conf.Get (Preference.VARIABLE_FONT, "Sans 12");
            string fixedFont = Conf.Get (Preference.FIXED_FONT, "Mono 12");

            // Disabled for now since it's not clear that overriding the
            // default font settings makes sense.
#if ENABLE_FONTS
            Pango.FontDescription varDesc = Pango.FontDescription.FromString (varFont);
						webSettings.DefaultFontFamily = varDesc.Family;
						//webSettings.DefaultFontSize = varDesc.Size / 1024;

            Pango.FontDescription fixedDesc = Pango.FontDescription.FromString (fixedFont);
						webSettings.MonospaceFontFamily = fixedDesc.Family;
						//webSettings.MonospaceFontSize = fixedDesc.Size / 1024;
#endif
        }

        private void FontNotifyHandler (object sender, NotifyEventArgs args)
        {
            if (args.Key == Conf.GetFullKey (Preference.VARIABLE_FONT) ||
                args.Key == Conf.GetFullKey (Preference.FIXED_FONT)) {
                SetFonts ();
            }
        }

        private void ProxyUpdatedCb ()
        {
            //Utils.GeckoSetProxy (Proxy.UseProxy, Proxy.ProxyHost,
            //                     Proxy.ProxyPort);
        }
    }

#if ENABLE_FONTS
    class WebSettings : WebKit.WebSettings {
        public WebSettings() {}
        
        public string DefaultFontFamily {
            get { return (string)GetProperty("default-font-family").Val; }
            set { SetProperty("default-font-family", new GLib.Value(value)); }
        }

        public int DefaultFontSize {
            get { return (int)GetProperty("default-font-size").Val; }
            set { SetProperty("default-font-size", new GLib.Value(value)); }
        }

        public string MonospaceFontFamily {
            get { return (string)GetProperty("monospace-font-family").Val; }
            set { SetProperty("monospace-font-family", new GLib.Value(value)); }
        }
    }
#endif
}
