//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

using GConf;
using Gdk;
using Gtk;
using GLib;
using GtkSharp;
using System;
using System.Collections;
using System.Threading;
using System.Threading.Tasks;
using System.ServiceModel.Syndication;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Imendio.Blam {
    
	public class ItemList : Gtk.TreeView, INotifyPropertyChanged {

		public event PropertyChangedEventHandler PropertyChanged;

	private ItemView itemView;
	
    private TreeViewColumn titleColumn;
    private TreeViewColumn iconColumn;
    CancellationTokenSource MarkReadCancel;

		IChannel _CurrentChannel;
		public IChannel CurrentChannel {
			get {
				return _CurrentChannel;
			}
			set {
				if (_CurrentChannel != value) {
					_CurrentChannel = value;
					UpdateList();
					NotifyPropertyChanged();
				}
			}
		}

		Item _Current;
		public Item Current {
			get {
				return _Current;
			}
			set {
				if (_Current != value) {
					_Current = value;
					NotifyPropertyChanged();
				}
			}
		}


		void NotifyPropertyChanged([CallerMemberName] string name = "")
		{
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}

	public ItemList(ItemView itemView, ChannelList channelList)
	{
	    this.itemView = itemView;
			channelList.ChannelSelectedEvent += channel => CurrentChannel = channel;

        CellRendererPixbuf cell2 = new CellRendererPixbuf();
        iconColumn = new TreeViewColumn();
        iconColumn.PackStart(cell2, true);
        iconColumn.Sizing = TreeViewColumnSizing.GrowOnly;
        iconColumn.Expand = false;
        iconColumn.SetCellDataFunc(cell2,
                   new TreeCellDataFunc(IconCellDataFunc));

        AppendColumn(iconColumn);

	    titleColumn = new TreeViewColumn();
	    CellRendererText cell = new CellRendererText();

	    titleColumn.PackStart(cell, true);
	    titleColumn.SetCellDataFunc(cell, 
					new TreeCellDataFunc(NameCellDataFunc));

	    AppendColumn(titleColumn);

	    this.Selection.Changed += new EventHandler(SelectionChanged);
	    this.Model = new ListStore (typeof(Imendio.Blam.Item));
	    this.HeadersVisible = false;

        (Model as ListStore).DefaultSortFunc = CompareFunc;
        SetSortOrder(Conf.Get(Preference.REVERSE_ENTRIES, false));

	    Conf.AddNotify (Conf.GetFullKey(Preference.REVERSE_ENTRIES), new NotifyEventHandler (ConfNotifyHandler));

	}

		bool Selected(out TreeModel model, out TreeIter iter)
		{
			return Selection.GetSelected(out model, out iter);
		}

		void ScrollTo(TreeIter iter)
		{
			GrabFocus();
			ScrollToCell(this.Model.GetPath(iter), titleColumn, false, 0, 0);
			SetCursor(this.Model.GetPath(iter), titleColumn, false);
		}

		bool LoopItems(TreeModel model, ref TreeIter iter, Func<Item, bool> fn)
		{
			var start = iter;
			do {
				Item item = (Item)model.GetValue(iter, 0);
				if (item != null && fn(item))
					return true;

				if (!Model.IterNext(ref iter))
					Model.GetIterFirst(out iter);
			} while (!iter.Equals(start));

			return false;
		}

		public bool Next(bool unread = true)
		{
			TreeModel model;
			TreeIter  iter;

			// If there's something selected (and it's not the last one),
			// start looking from there. Otherwise from the start.
			if (!Selected(out model, out iter) || !Model.IterNext(ref iter))
				((ListStore)Model).GetIterFirst(out iter);

			Func<Item, bool> check;
			if (unread)
				check = item => item.Unread;
			else
				check = (_) => true;

			if (LoopItems(model, ref iter, check)) {
				ScrollTo(iter);
				return true;
			}

			return false;
		}

		public void UpdateList()
		{
			if (CurrentChannel == null)
				return;

			var list = Model as ListStore;

			list.Clear();
			foreach(string id in CurrentChannel.ItemList){
				if (id == null)
					continue;

				list.AppendValues(ItemStore.Get(id));
			}
		}

	public void Update (Item item)
	{
	    TreeIter iter = FindItem (item);
	    
	    if (!iter.Equals (TreeIter.Zero)) {
		this.Model.EmitRowChanged (this.Model.GetPath(iter), iter);
	    }
	}

		public Item GetSelected()
		{
			TreeIter iter;
			TreeModel model;

			if (Selected(out model, out iter))
				return (Item)model.GetValue(iter, 0);

			return null;
		}

		public void EmitSelectedRowChanged()
		{
			TreeIter iter;
			TreeModel model;

			if (!Selected(out model, out iter))
				return;

			model.EmitRowChanged(model.GetPath(iter), iter);
		}

		private async void SelectionChanged (object obj, EventArgs args)
		{
			TreeSelection selection = (TreeSelection) obj;
			Imendio.Blam.Item item;

			item = selection.Item();
			if (item == null)
				return;

			if (MarkReadCancel != null)
				MarkReadCancel.Cancel();

			Current = item;

			bool useTimeout = (bool) Conf.Get(Preference.MARK_ITEMS_READ, false);

			if (!useTimeout) {
				item.Unread = false;
				selection.EmitRowChanged();
				return;
			}

			MarkReadCancel = new CancellationTokenSource();
			var readTimeout = (uint) Conf.Get(Preference.MARK_ITEMS_READ_TIMEOUT, 3000);

			try {
				await Task.Delay(TimeSpan.FromMilliseconds(readTimeout), MarkReadCancel.Token);
			} catch (OperationCanceledException) {
				return;
			}

			MarkReadCancel = null;
			item.Unread = false;
			selection.EmitRowChanged();
		}

		private int CompareFunc(TreeModel model, TreeIter a, TreeIter b)
		{
			Item ia = Model.GetValue(a, 0) as Item;
			Item ib = Model.GetValue(b, 0) as Item;

			DateTimeOffset maxa = ia.PublishDate.CompareTo(ia.LastUpdatedTime) > 0 ? ia.PublishDate : ia.LastUpdatedTime;
			DateTimeOffset maxb = ib.PublishDate.CompareTo(ib.LastUpdatedTime) > 0 ? ib.PublishDate : ib.LastUpdatedTime;

			return maxa.CompareTo(maxb);
		}

	protected override bool OnKeyPressEvent (EventKey kEvent)
	{
	    switch (kEvent.Key) {
	    case Gdk.Key.space:
	    case Gdk.Key.Page_Up:
	    case Gdk.Key.Page_Down:
		itemView.Widget.ProcessEvent (kEvent);
		return false;
		// return itemView.OnKeyPressEvent (kEvent);
	    default:
		return base.OnKeyPressEvent (kEvent);
	    }
	}

	private void NameCellDataFunc (TreeViewColumn col,
				       CellRenderer   cell,
				       TreeModel      model,
				       TreeIter       iter)
	{
	    Imendio.Blam.Item item = (Imendio.Blam.Item)model.GetValue(iter, 0);
	    
	    int weight = (int)Pango.Weight.Normal;

	    if (item.Unread) {
		weight = (int) Pango.Weight.Bold;
	    }


        ((CellRendererText)cell).Text = HtmlUtils.Unescape((item.Title as TextSyndicationContent).Text.Trim());
        ((CellRendererText)cell).Weight = weight;
        ((CellRendererText)cell).Ellipsize = Pango.EllipsizeMode.End;
	}

    private void IconCellDataFunc(TreeViewColumn col,
                       CellRenderer   cell,
				       TreeModel      model,
				       TreeIter       iter)
    {
        Item item = model.GetValue(iter, 0) as Imendio.Blam.Item;
        string icon = null;

        if(item.Unread == true){
            icon = "gnome-stock-book-red";
        } else {
            icon = "gnome-stock-book-open";
        }

        if(item.Old == true && item.Unread == true){
            icon = "gnome-stock-book-green";
        }

        if((cell as CellRendererPixbuf).IconName != icon){
            (cell as CellRendererPixbuf).IconName = icon;
        }
    }
	
	private void SetSortOrder (bool reverseEntries)
	{
	    SortType sortType = SortType.Ascending;

	    if (reverseEntries) {
		sortType = SortType.Descending;
	    }
	    
	    ((ListStore)this.Model).SetSortColumnId (-1, sortType);
	}

	private void ConfNotifyHandler (object sender, NotifyEventArgs args)
	{
	    if (args.Key == Conf.GetFullKey (Preference.REVERSE_ENTRIES)) {
		SetSortOrder ((bool) args.Value);
	    }
	}

	// Used by Updated
	private Item     findItem;
	private TreeIter foundIter;

	private bool ForeachFindItem (TreeModel model, 
				      TreePath  path,
				      TreeIter  iter)
	{
	    Item item = (Item) model.GetValue (iter, 0);
	    
	    if (item == findItem) {
		foundIter = iter;
		return true;
	    }

	    return false;
	}

	private TreeIter FindItem (Item item)
	{
	    findItem = item;
	    foundIter = TreeIter.Zero;

	    this.Model.Foreach (new TreeModelForeachFunc (ForeachFindItem));

	    return foundIter;
	}
    }
}

