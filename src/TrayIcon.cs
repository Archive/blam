//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using Gtk;
using Mono.Unix;
using System;
using System.ComponentModel;

namespace Imendio.Blam {
    public class TrayIcon {
    private Gtk.StatusIcon mIcon = null;
    ChannelCollection Collection { get; set; }

	public event EventHandler RefreshAllEvent;
	public event EventHandler PreferencesEvent;
	public event EventHandler AboutEvent;
	public event EventHandler QuitEvent;

	public event EventHandler ButtonPressEvent;

	public string Tooltip {
	    set {
		    mIcon.Tooltip = value;
	    }
	}

	public TrayIcon (string name, ChannelCollection collection)
	{
        mIcon = new Gtk.StatusIcon();
        mIcon.IconName = "blam";
        mIcon.Tooltip = name;
        mIcon.Activate += ButtonPressedCb;
        mIcon.PopupMenu += PopupCb;
			Collection = collection;
        Collection.PropertyChanged += HandlePropertyChanged;
			Collection.CollectionChanged += HandleCollectionChanged;
	}

		void HandleCollectionChanged (object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			UpdateTooltip();
		}

		void HandlePropertyChanged (object sender, PropertyChangedEventArgs e)
		{
			UpdateTooltip();
		}
	
		void UpdateTooltip()
		{
			var nrUnread = Collection.NrOfUnreadItems;
			var nrNew = Collection.NrOfNewItems;

			var strUnread = string.Format(Catalog.GetPluralString ("{0} unread item", "{0} unread items", nrUnread), nrUnread);
			var strNew = string.Format(Catalog.GetPluralString("({0} new)", "({0} new)", nrNew), nrNew);
			var tooltip = strUnread + " " + strNew;

			Tooltip = tooltip;
		}

    public void Hide()
    {
        mIcon.Visible = false;
    }

	public void Show ()
	{
	    mIcon.Visible = true;
	}

	private void ButtonPressedCb (object o, EventArgs args)
	{
	    if (this.ButtonPressEvent != null) {
		this.ButtonPressEvent (o, args);
	    }
	}
	/*
	 * Create a popup menu
	 */
	public void PopupCb(object o, EventArgs args)
	{
		Gtk.Menu menu = new Gtk.Menu();
		Gtk.ImageMenuItem refreshItem = new Gtk.ImageMenuItem(Catalog.GetString("_Refresh"));
		refreshItem.Image = new Gtk.Image(Stock.Refresh,IconSize.Menu);
		refreshItem.Activated += RefreshAllEvent;

		Gtk.ImageMenuItem prefItem = new Gtk.ImageMenuItem(Catalog.GetString("Preferences"));
		prefItem.Image = new Gtk.Image(Stock.Preferences,IconSize.Menu);
		prefItem.Activated += PreferencesEvent;

		Gtk.ImageMenuItem aboutItem = new Gtk.ImageMenuItem(Catalog.GetString("About Blam"));
		aboutItem.Image = new Gtk.Image(Stock.About,IconSize.Menu);
		aboutItem.Activated += AboutEvent;

		Gtk.ImageMenuItem quitItem = new Gtk.ImageMenuItem(Catalog.GetString("Quit"));
		quitItem.Image = new Gtk.Image(Stock.Quit,IconSize.Menu);
		quitItem.Activated += QuitEvent;

		menu.Add(refreshItem);
		menu.Add(prefItem);
		menu.Add(new Gtk.SeparatorMenuItem());
		menu.Add(aboutItem);
		menu.Add(new Gtk.SeparatorMenuItem());
		menu.Add(quitItem);
		menu.ShowAll();
		menu.Popup();
	}
    }
}
