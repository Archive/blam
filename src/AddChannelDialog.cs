using System;
using System.Linq;
using Gtk;

namespace Imendio.Blam
{
    public partial class AddChannelDialog : Gtk.Dialog
    {
		bool OkSensitive()
		{
			return !string.IsNullOrEmpty(Url);
		}

        public AddChannelDialog(Gtk.Window window)
        {
            this.Build();
            this.TransientFor = window;
            buttonOk.Sensitive = false;
            var clipboard = Gtk.Clipboard.Get(Gdk.Atom.Intern("CLIPBOARD", true));
            clipboard.RequestText((c, text) => {
                if (!String.IsNullOrEmpty(text) && new[] {"http://", "https://"}.Any((p) => text.StartsWith(p))) {
                    Url = text;
                } else {
                    Url = String.Empty;
                }
            });

            buttonOk.Sensitive = OkSensitive();
            UrlEntry.Activated += OnEntryActivated;
            UsernameEntry.Activated += OnEntryActivated;
            PasswordEntry.Activated += OnEntryActivated;

            UrlEntry.Changed += (sender, e) => buttonOk.Sensitive = OkSensitive();
        }

        public string Username {
            get { return UsernameEntry.Text; }
        }

        public string Password {
            get { return PasswordEntry.Text; }
        }

        public string Url {
            get { return UrlEntry.Text; }
            set { UrlEntry.Text = value; }
        }

        protected void OnEntryActivated (object sender, EventArgs e)
        {
            if (buttonOk.Sensitive)
                buttonOk.Click();
        }
    }
}

