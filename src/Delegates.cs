//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

namespace Imendio.Blam {
    public delegate void BlamEventHandler      ();
    public delegate void ChannelEventHandler   (IChannel channel);
    public delegate void ChannelGroupEventHandler (IChannel group, IChannel channel);
    public delegate void ActionFinishedHandler (string  status);
    public delegate void StringUpdatedHandler  (string  str);
}
