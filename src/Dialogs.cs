//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using Glade;
using Gtk;
using Gdk;
using Mono.Unix;
using System;

namespace Imendio.Blam {
    class AddGroupDialog
    {
        [Widget] Gtk.Dialog addGroupDialog = null;
        [Widget] Gtk.Entry groupName       = null;
        [Widget] Gtk.Button ok             = null;

        Application mApp = null;

        public AddGroupDialog(Application app)
        {
            mApp = app;

            Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
                                 "addGroupDialog", null);
            gladeXML.Autoconnect(this);
            addGroupDialog.TransientFor = app.Window;
            addGroupDialog.IconName = "blam";
        }

        public void Show()
        {
            groupName.Text = "";
            addGroupDialog.ShowAll();
        }

        public void OkButtonClicked(object obj, EventArgs args)
        {
            ChannelGroup group = new ChannelGroup();
            group.Name = groupName.Text;
            mApp.CCollection.Groups.Add(group);
            Gtk.TreeIter iter = (mApp.ChannelList.Model as TreeStore).AppendValues(group);
            group.Iter = iter;

            addGroupDialog.Hide();
        }

        public void CancelButtonClicked(object obj, EventArgs args)
        {
            addGroupDialog.Hide();
        }
    }

    class RemoveChannelDialog {
	[Widget] Gtk.Dialog removeChannelDialog = null;
	[Widget] Gtk.Label  dialogTextLabel     = null;

	private ChannelCollection mCollection;

	private static RemoveChannelDialog removeDialog = null;
	
	private RemoveChannelDialog(Gtk.Window parent,
				    ChannelCollection collection)
	{
	    mCollection = collection;
	    Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
							"removeChannelDialog",
							null);
	    gladeXML.Autoconnect(this);
	    removeChannelDialog.TransientFor = parent;
	    removeChannelDialog.IconName = "blam";
	}

	public static void Show (Gtk.Window parent, ChannelCollection collection, IChannel channel)
	{
	    if (removeDialog == null) {
		removeDialog = new RemoveChannelDialog (parent, collection);
	    }

	    string name = "<b>" + channel.Name + "</b>";
	  
	    string str = String.Format (Catalog.GetString ("Do you want to remove the channel or group {0} from the channel list?"), name);

	    removeDialog.dialogTextLabel.Markup = str;
	    
	    int response = removeDialog.removeChannelDialog.Run ();
	    removeDialog.removeChannelDialog.Hide ();
	    
	    switch (response) {
	    case (int) ResponseType.Cancel:
		return;
	    case (int) ResponseType.Ok:
		removeDialog.mCollection.Remove (channel);
		return;
	    }
	}
    }
}

