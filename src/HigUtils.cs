//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using Glade;
using Gtk;
using System;
using System.Runtime.InteropServices;

namespace Imendio.Blam {
    
    public class ErrorDialog {
        [Widget] Gtk.Dialog errorDialog = null;
        [Widget] Gtk.Label HeadLabel = null;
        [Widget] Gtk.Label MessageLabel = null;
        
        public ErrorDialog(Gtk.Window window, string header, string msg)
        {
            Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
                                                        "errorDialog", null);
            gladeXML.Autoconnect(this);
            
            HeadLabel.Markup = "<span weight=\"bold\" size=\"larger\">" + header + "</span>";
            MessageLabel.Text  = msg;
            errorDialog.TransientFor = window;
        }
        
        public static Gtk.Dialog Create(Gtk.Window w, string h, string m)
        {
            ErrorDialog d = new ErrorDialog(w, h, m);
            return d.errorDialog;
            
        }
    }
    
    public class ConfirmationDialog {
	[Widget] Gtk.Dialog confirmationDialog = null;
	[Widget] Gtk.Label  primaryLabel       = null;
	[Widget] Gtk.Label  secondaryLabel     = null;
	[Widget] Gtk.Button okButton           = null;
    
	private ConfirmationDialog (Gtk.Window parentWindow, string okButtonText, string primaryText, string secondaryText)
	{
            Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
                                                        "confirmationDialog", null);
            gladeXML.Autoconnect(this);
	    
	    confirmationDialog.TransientFor = parentWindow;
	    okButton.Label = okButtonText;

	    primaryLabel.Markup = "<span weight=\"bold\" size=\"larger\">" + primaryText + "</span>";

	    secondaryLabel.Text = secondaryText;
	}
	    
	
	public static Gtk.Dialog Create (Gtk.Window parentWindow, string okButtonText, string primaryText, string secondaryText)
	{
	    ConfirmationDialog dialog = new ConfirmationDialog (parentWindow, okButtonText, primaryText, secondaryText);

	    return dialog.confirmationDialog;
	}
    }
}
