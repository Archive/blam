//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2005 Imendio AB
// (C) 2005 Michael Ostermeier

using System.IO;
using System.Collections.Generic;

namespace Imendio.Blam {
	public class Theme {
		private string mName = "";
		private string mPath = "";

		private string mThemeHtml = "";

		public Theme (string path)
		{
			mPath = path;

            Load ();
		}

		public string Name {
			get {
				return mName;
			}
		}

		public string Path {
			get {
				return mPath;
			}
		}

		public string Render(Dictionary<string,string> replaces)
		{
			string str = mThemeHtml;

			foreach (var k in replaces.Keys)
				str = str.Replace("${" + k + "}", replaces[k]);

			return str;
		}

		private void Load () 
		{
            string fileName = mPath + "/" + Defines.THEME_INDEX_NAME;
            TextReader r = File.OpenText (fileName);
            mThemeHtml = r.ReadToEnd ();
            r.Close ();

            // TODO: Add better error handling here
            mName = mPath.Substring (mPath.LastIndexOf ("/") + 1);
		}
	}
}
