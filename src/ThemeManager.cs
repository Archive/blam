//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2005 Imendio AB
//

using GConf;
using System;
using System.Collections;
using System.IO;

namespace Imendio.Blam {
	
	public class ThemeManager {
		private IList mThemes;
        private Theme mCurrentTheme;

		public delegate void ThemeSelectedHandler (Theme theme);
		public event ThemeSelectedHandler ThemeSelected;

        public Theme CurrentTheme {
            get {
                return mCurrentTheme;
            }
        }

		public ThemeManager ()
		{
			string themePath;

			mCurrentTheme = null;
            mThemes = new ArrayList ();

			try {
				LoadThemes (Defines.PERSONAL_THEME_DIR);
				LoadThemes (Defines.THEME_DIR);
			} catch (Exception) {
				/* Do nothing */
			}
		
			themePath = Conf.Get (Preference.THEME, Defines.DEFAULT_THEME);
			SetTheme (themePath);

			Conf.AddNotify (Conf.GetFullKey(Preference.THEME), ConfNotifyHandler);
		}

        // Returns a list of Themes
		public IList GetThemeList ()
		{
            return mThemes;
		}

		public Theme AddTheme (string path)
		{
			Theme theme = new Theme (path);
			mThemes.Add (theme);

			return theme;
		}

		public string PathByName(string name)
		{
			foreach(Theme t in mThemes){
				if(t.Name == name){
					return t.Path;
				}
			}
			return name;
		}

		// -- Private functions --
		private void LoadThemes (string path)
		{
			try {
				string[] dirs = Directory.GetDirectories (path);
				
			foreach (string dir in dirs) {
				try {
					Theme theme = new Theme (dir);
					mThemes.Add (theme);
				} catch (Exception) {
					/* Do nothing, just continue to the next one */
				}
			}
			} catch (DirectoryNotFoundException) {
				/* This dir doesn't exist, no sweat. */
				return;
			}
		}  

		private void SetTheme (string themePath)
		{
			foreach (Theme t in mThemes) {
				if (t.Path == themePath) {
					SetTheme (t);
					return;
				}
			}

			/* Theme didn't exist, we create it and then set it */
					try {
				Theme theme = AddTheme (themePath);
				SetTheme (theme);
			} catch (Exception) {
				if (mCurrentTheme == null) {
					SetTheme (Defines.DEFAULT_THEME);
				} else {
					SetTheme (mCurrentTheme);
				}
			}
		}

		private void SetTheme (Theme theme)
		{
			if(Conf.Get(Preference.THEME, "") != theme.Path){
				Conf.Set (Preference.THEME, theme.Path);
			}
			if (theme == mCurrentTheme) {
				return;
			}

			mCurrentTheme = theme;
			EmitThemeSelected (theme);
		}

		private void EmitThemeSelected (Theme theme)
		{
			if (ThemeSelected != null) {
				ThemeSelected (theme);
			}
		}

		private void ConfNotifyHandler (object sender, NotifyEventArgs args)
		{
			if (args.Key == Conf.GetFullKey (Preference.THEME)) {
				SetTheme ((string) args.Value);
			}
		}
	}
}

