//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

using GConf;
using System;

namespace Imendio.Blam {

    public class Conf {
	public static string GConfBase = "/apps/blam";
	public static GConf.Client GConfClient;

	public static int Get (string key, int defaultVal)
	{
	    try {
		EnsureClient ();
		
		return (int) GConfClient.Get (GetFullKey (key));
	    } catch {}
	    
	    return defaultVal;
	}

	public static string Get (string key, string defaultVal)
	{
	    try {
		EnsureClient ();
		
		return (string) GConfClient.Get (GetFullKey (key));
	    } catch {}
	    
	    return defaultVal;
	}
	
	public static bool Get (string key, bool defaultVal)
	{
	    try {
		EnsureClient ();
		
		return (bool) GConfClient.Get (GetFullKey (key));
	    } catch {}
	    
	    return defaultVal;
	}

	public static void Set (string key, int value)
	{
	    Set (key, (object) value);
	}

	public static void Set (string key, string value)
	{
	    Set (key, (object) value);
	}

	public static void Set (string key, bool value)
	{
	    Set (key, (object) value);
	}

	private static void Set (string key, object value)
	{
	    EnsureClient ();

	    GConfClient.Set(GetFullKey (key), value);
	}

	public static void AddNotify (NotifyEventHandler handler)
	{
	    EnsureClient ();

	    GConfClient.AddNotify (GConfBase, handler);
	}

	public static void AddNotify (string baseDir, NotifyEventHandler handler)
	{
	    EnsureClient ();

	    GConfClient.AddNotify (baseDir, handler);
	}

	public static void Sync ()
	{
	    EnsureClient ();
	    
	    GConfClient.SuggestSync ();
	}

	public static string GetFullKey (string key)
	{
	    if (key.StartsWith ("/")) {
		return key;
	    }

	    return GConfBase + "/" + key;
	}

	private static void EnsureClient ()
	{
	    if (GConfClient == null) {
		GConfClient = new GConf.Client();
	    }
	}

    }
}
