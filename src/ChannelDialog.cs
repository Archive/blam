//
// Author:
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
//

using Gdk;
using Gtk;
using Glade;
using System;
using System.Text;

namespace Imendio.Blam {
    public class ChannelDialog {
    [Widget] Gtk.Dialog channelDialog = null;
    [Widget] Gtk.Entry  nameEntry     = null;
    [Widget] Gtk.Entry  urlEntry      = null;
    [Widget] Gtk.Entry  usernameEntry = null;
    [Widget] Gtk.Entry  passwordEntry = null;
    [Widget] Gtk.Entry  keywordEntry  = null;
    [Widget] Gtk.Button okButton      = null;
    [Widget] Gtk.Image  dialogImage   = null;

	private Channel     mChannel;
	
	private Application mApp;

	public string Name {
	    get {
		return nameEntry.Text;
	    }
	    set {
		nameEntry.Text = value;
	    }
	}
	
	public string Url {
	    get {
		return urlEntry.Text;
	    }
	    set {
		urlEntry.Text = value;
	    }
	}
	
	public ChannelDialog (Application app)
	{
	    mApp = app;
	    
	    Glade.XML gladeXML = Glade.XML.FromAssembly("blam.glade",
							"channelDialog",
							null);
	    gladeXML.Autoconnect(this);
	    channelDialog.TransientFor = mApp.Window;
	    channelDialog.IconName = "blam"; 
	    
	    dialogImage.Pixbuf =
		Gdk.Pixbuf.LoadFromResource ("blam-edit-news.png");
	}

	public void Show (Channel channel)
	{
	    mChannel = channel;
	    
	    nameEntry.Text = channel.Name;
	    urlEntry.Text = channel.Url;
	    keywordEntry.Text = channel.Keywords;
	    usernameEntry.Text = channel.http_username;
	    passwordEntry.Text = channel.http_password;

	    channelDialog.ShowAll ();
	}
	
	public void CancelButtonClicked (object obj, EventArgs args)
	{
	    channelDialog.Hide();
	}

	public void OkButtonClicked (object obj, EventArgs args)
	{
	    mChannel.Name = nameEntry.Text;

	    if (urlEntry.Text != mChannel.Url) {
	        // Re-detect feed type on next refresh
	        mChannel.Type = "";
	    }
	    mChannel.Url = urlEntry.Text;
	    mChannel.Keywords = keywordEntry.Text;
	    mChannel.http_username = usernameEntry.Text;
	    mChannel.http_password = passwordEntry.Text;

	    channelDialog.Hide ();
	}

	public void EntryChanged (object obj, EventArgs args)
	{
	    if (!nameEntry.Text.Equals("") && !urlEntry.Text.Equals("")) {
		okButton.Sensitive = true;
	    } else {
		okButton.Sensitive = false;
	    }
	}

	public void EntryActivated (object obj, EventArgs args)
	{
	    if (!nameEntry.Text.Equals("") && !urlEntry.Text.Equals("")) {
		okButton.Click();
	    }
	}
    }
}
