/*
 * Copyright (c) 2008 Carlos Martín Nieto <carlos@cmartin.tk>
 * 
 * This file is released under the GNU GPL v2 or later.
 */

using System;
using System.Collections;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.Linq;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Imendio.Blam
{
    [XmlType("group")]
    public class ChannelGroup : IChannel, INotifyPropertyChanged
    {
        [XmlAttribute("Name")] public string Int_Name = null;
        [XmlAttribute("Url")] public string Int_Url = null;
        [XmlElement("Channel", typeof(Channel))] public ArrayList Channels;
        private Gtk.TreeIter mIter;

		public event PropertyChangedEventHandler PropertyChanged;

        public ArrayList ItemList {
            get { /* FIXME: Cache this value. */
                ArrayList tmp = new ArrayList();
                foreach(Channel chan in Channels){
                    tmp.AddRange(chan.ItemList);
                }
                return tmp;
            }
            set {}
        }
        
        public int NrOfUnreadItems {
            get {
                int nr = 0;

                if(Channels.Count == 0)
                    return nr;

                foreach(IChannel channel in Channels){
                    nr += channel.NrOfUnreadItems;
                }

                return nr;
            }
        }

        public int NrOfNewItems {
            get {
                int nr = 0;

                if(Channels.Count == 0)
                    return nr;

                foreach(IChannel channel in Channels){
                    nr += channel.NrOfUnreadItems;
                }

                return nr;
            }
        }

        public int NrOfItems {
            get {
                int n = 0;

                if(Channels.Count == 0)
                    return n;

                foreach(IChannel channel in Channels){
                    n += channel.NrOfItems;
                }
                
                return n;
            }
        }
        public string Name {
            get {
                return Int_Name;
            }
            set {
                Int_Name = value;
            }
        }

        public string Url {
            get {
                return Int_Url;
            }
            set {
                Int_Url = value;
            }
        }

        [XmlIgnore]
        public Gtk.TreeIter Iter {
            get {
                return mIter;
            }
            set {
                mIter = value;
            }
        }

		public void Add(IChannel chan)
		{
			foreach(Channel ch in Channels){
				if(ch.Url == chan.Url)
					return;
			}

			if(chan.Name == null)
				chan.Name = chan.Url;

			Channels.Add(chan);
		}

		public void RemoveItems()
		{
			foreach(Channel chan in Channels){
				chan.RemoveItems();
			}
		}

        public ChannelGroup() : base()
        {
            if(Channels == null)
                Channels = new ArrayList();
        }

		void ChannelChanged(object sender, PropertyChangedEventArgs args)
		{
			// Whatever's changed in the children has changed for us as well
			NotifyPropertyChanged(args.PropertyName);
		}

		void NotifyPropertyChanged([CallerMemberName] string name = "")
		{
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}

        public void Setup()
        {
            foreach(Channel chan in Channels){
                chan.Setup();
				chan.PropertyChanged += ChannelChanged;
            }
        }

        public bool MarkAsRead()
        {
            bool ret = false;

            foreach(Channel chan in Channels){
                ret = chan.MarkAsRead();
            }
            return ret; /* FIXME: We shoudl probably do this some other way. */
        }

        public Item GetItem(string id)
        {
            Item item = null;
            foreach(IChannel ch in Channels){
                item = ch.GetItem(id);
                if(item != null){
                    return item;
                }
            }
            return null;
        }

		public async Task<bool> RefreshAsync()
		{
			var tasks = Channels.Cast<Channel>().Select(c => c.RefreshAsync());
			// FIXME: The continuation is a horrible hack to fit the interface
			await Task.WhenAll(tasks);
			return true;
		}
    }
}
