using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.ServiceModel.Syndication;
using System.Xml;

namespace Imendio.Blam
{
    public class ItemStore
    {
		static ConcurrentDictionary<string, Item> Items;
        static string itemfile = Defines.APP_HOMEDIR + "/" + Defines.APP_ITEMSTORE_FILE;
        static string itemfile_tmp = Defines.APP_HOMEDIR + "/" + Defines.APP_ITEMSTORE_FILE + ".tmp";

        public static Item Get(string id)
        {
            Item item;

            if(id == null){
                Console.Error.WriteLine("Tried to access item with null key");
                return null;
            }

			if (!Items.TryGetValue(id, out item))
				return null;

			return item;
        }

        public static void Add(Item item)
        {
            if(item.Id == null){
                Console.Error.WriteLine("Tried to add item with null key");
                return;
            }

			Item inside;

			// Add the item if it's not there already and increase the refcount
			// of whatever is or was inside.
			inside = Items.GetOrAdd(item.Id, item);
			inside.Grab();
        }

        public static void Remove(Item item)
        {
            if(item == null){
                return;
            }

			// FIXME: pretty sure this is a race condition
			item.Release();
			if (item.RefCount == 0)
				Items.TryRemove(item.Id, out item);
		}

        public static void Remove(string id)
        {
			Remove(Items[id]);
        }

		public static void Load()
        {
			Items = new ConcurrentDictionary<string, Item>();

            XmlReader reader;
            try{
                reader = XmlReader.Create(itemfile);
            } catch(Exception e){
                Console.WriteLine("Can't open item db: {0}", e.Message);
                return;
            }

            SyndicationFeed feed = SyndicationFeed.Load(reader);
			var kv = feed.Items.Select(i => new KeyValuePair<string, Item>(i.Id, new Item(i)));
			Items = new ConcurrentDictionary<string, Item>(kv);
        }

        public static void Save()
        {
			var values = Items.Values;
			foreach (var item in values)
				item.WriteExtensions();

			var items = new Item[Items.Count];
			values.CopyTo(items, 0);

            XmlWriter writer = XmlWriter.Create(itemfile_tmp);
            SyndicationFeed sf = new SyndicationFeed(items);
            Atom10FeedFormatter fmtr = sf.GetAtom10Formatter();
            fmtr.WriteTo(writer);
            writer.Close();
            File.Delete(itemfile);
            File.Move(itemfile_tmp, itemfile);
        }
    }
}
