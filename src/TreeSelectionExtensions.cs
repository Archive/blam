using System;
using Gtk;

namespace Imendio.Blam
{
	public static class TreeSelectionExtensions
	{
		public static void EmitRowChanged(this TreeSelection selection)
		{
			TreeIter iter;
			TreeModel model;

			if (!selection.GetSelected (out model, out iter)) {
				return;
			}

			model.EmitRowChanged(model.GetPath(iter), iter);
		}

		public static Item Item(this TreeSelection selection)
		{
			TreeIter iter;
			TreeModel model;

			if (!selection.GetSelected (out model, out iter)) {
				return null;
			}

			return (Imendio.Blam.Item) model.GetValue (iter, 0);
		}
	}
}

