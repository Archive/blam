// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio AB
// 

using System.Xml.Serialization;
using System;
using System.ComponentModel;
using System.Threading;
using System.ServiceModel.Syndication;
using System.Runtime.CompilerServices;

namespace Imendio.Blam {
	public class Item : SyndicationItem, INotifyPropertyChanged {
		public bool     Old = false;
		public bool     Permanent = false;

		public string   keywords = "";
		public event PropertyChangedEventHandler PropertyChanged;

		bool unread = true;
		public bool Unread {
			get {
				return unread;
			}
			set {
				if (unread != value) {
					unread = value;
					NotifyPropertyChanged();
				}
			}
		}

        private int ref_cnt = 0;
        private Object obj = new Object();

		public string Keywords {
			get {
				return keywords;
			}
			set {
				if (value.Equals ("")) {
					this.Permanent = false;
				} else {
					this.Permanent = true;
				}

				this.keywords = value;
			}
		}

        public void WriteExtensions()
        {
            ElementExtensions.Clear();
            ElementExtensions.Add("unread", "blam", Unread);
            ElementExtensions.Add("old", "blam", Old);
            ElementExtensions.Add("permanent", "blam", Permanent);
            ElementExtensions.Add("keywords", "blam", keywords);
            ElementExtensions.Add("refcount", "blam", ref_cnt);
        }

        public int RefCount {
            get {
                return ref_cnt;
            }
        }

        public void Grab()
        {
            Interlocked.Increment(ref ref_cnt);
        }

        public void Release()
        {
            Interlocked.Decrement(ref ref_cnt);
        }

		public Item ()
		{
		}

		void NotifyPropertyChanged([CallerMemberName] string name = "")
		{
			if (PropertyChanged != null) {
				PropertyChanged(this, new PropertyChangedEventArgs(name));
			}
		}

        private void FillItem(SyndicationItem item)
        {
            if(this.Id == null){
                if(item.Id == null)
                    this.Id = item.Links[0].Uri.ToString();
                else
                    this.Id = item.Id;
            }

            /* Newlines aren't allowed in titles */
            this.Title = new TextSyndicationContent(HtmlUtils.CollapseWhitespace(item.Title.Text));

            if(item.Content != null){
                /* The feed formatters only store the summary */
                this.Summary = item.Content as TextSyndicationContent;
            }

            foreach(SyndicationElementExtension ext in item.ElementExtensions){
                if(ext.OuterName == "encoded"){
                    this.Summary = new TextSyndicationContent(ext.GetObject<string>());
                }

                switch(ext.OuterNamespace){
                   /* Read our Item properties */
                    case "blam":
                        switch(ext.OuterName){
                    case "unread":
                        Unread = ext.GetObject<bool>();
                        break;
                    case "old":
                        Old = ext.GetObject<bool>();
                        break;
                    case "permanent":
                        Permanent = ext.GetObject<bool>();
                        break;
                    case "keywords":
                        Keywords = ext.GetObject<string>();
                        break;
                    case "refcount":
                        ref_cnt = ext.GetObject<int>();
                        break;
                    }
                    break;
                }
            }
        }

        public Item(SyndicationItem item) : base(item)
        {
            lock(obj){
                FillItem(item);
            }
        }

        public bool Update(SyndicationItem item)
        {
            lock(obj){
                FillItem(item);
            }
            return true;
        }
	}
}
