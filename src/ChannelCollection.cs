//
// Author: 
//   Mikael Hallendal <micke@imendio.com>
//
// (C) 2004 Imendio HB
// 

using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.ComponentModel;

namespace Imendio.Blam {

	public class ChannelCollection : INotifyPropertyChanged, INotifyCollectionChanged {

		public event NotifyCollectionChangedEventHandler CollectionChanged;
		public event PropertyChangedEventHandler PropertyChanged;
	
	public event ChannelEventHandler ChannelRefreshStarted;
	public event ChannelEventHandler ChannelRefreshFinished;

	public string FileName;

	private Object clock = new Object();
	Task DelayedWrite;

	private static uint WRITE_TIMEOUT = 5 * 60 * 1000; // Every 5 minutes
	
	static XmlSerializer serializer = new XmlSerializer (typeof (ChannelCollection));
	
	private ArrayList mChannels;
	[XmlElement ("Channel", typeof (Channel))]
	public ArrayList Channels {
	    get {
		return mChannels;
	    }
	    set {
		mChannels = value;
	    }
	}

	[XmlElement("Group", typeof(ChannelGroup))] public ArrayList Groups;

	public int NrOfUnreadItems {
	    get {
		int unread = 0;

		foreach (Channel channel in mChannels) {
		    unread += channel.NrOfUnreadItems;
		}

        foreach (ChannelGroup changrp in Groups) {
            unread += changrp.NrOfUnreadItems;
        }
		
		return unread;
	    }
	}

        public int NrOfNewItems {
            get {
                int new_items = 0;

                foreach(Channel channel in mChannels){
                    new_items += channel.NrOfNewItems;
                }

                foreach(ChannelGroup changrp in Groups){
                    new_items += changrp.NrOfNewItems;
                }

                return new_items;
            }
        }

		void NotifyAdd(IChannel channel)
		{
			NotifyCollectionChanged(NotifyCollectionChangedAction.Add, channel);
		}

		void NotifyAdd(object obj)
		{
			NotifyCollectionChanged(NotifyCollectionChangedAction.Add, obj);
		}

		void NotifyRemove(IChannel channel)
		{
			NotifyCollectionChanged(NotifyCollectionChangedAction.Remove, channel);
		}

		void NotifyChange(IChannel channel)
		{
			NotifyCollectionChanged(NotifyCollectionChangedAction.Replace, channel);
		}

		void NotifyCollectionChanged(NotifyCollectionChangedAction action, object obj)
		{
			if (CollectionChanged != null)
				CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, obj));
		}

	public ChannelCollection ()
	{
	}

	public static ChannelCollection LoadFromFile (string file)
	{
	    ChannelCollection collection;

	    try {
				Console.WriteLine("trying to load from file");
		collection = RealLoadFromFile (file);
	    } catch (Exception e) {
			Console.WriteLine("failed: {0}", e);
		try {
		    collection = RealLoadFromFile (Defines.APP_DATADIR + "/collection.xml");
		} catch {
		    collection = new ChannelCollection ();
		    collection.mChannels = new ArrayList ();
            collection.Groups = new ArrayList ();
		}
	    }

	    collection.FileName = file;
            foreach(ChannelGroup chan in collection.Groups){
                chan.PropertyChanged += collection.ChannelChanged;
                chan.Setup();
            }
            foreach(Channel chan in collection.mChannels){
				chan.PropertyChanged += collection.ChannelChanged;
                chan.Setup();
            }

            return collection;
	}

	private static ChannelCollection RealLoadFromFile (string file)
	{
	    ChannelCollection collection;
	    XmlTextReader reader = null;

	    try {
		reader = new XmlTextReader (file);
	    } catch (Exception) {
		reader = new XmlTextReader (GetTempFile (file));
	    }
	    
            collection = (ChannelCollection) serializer.Deserialize (reader);
            
	    reader.Close ();

            if (collection.Channels == null) {
                collection.mChannels = new ArrayList ();
            }

	    return collection;
	}
	
	public void SaveToFile ()
	{
	    lock (clock) {

		string tmpFile = GetTempFile (this.FileName);

		try {
		    Stream writer = new FileStream (tmpFile, FileMode.Create);
		    serializer.Serialize (writer, this);
		    writer.Close();
		} catch (Exception) {
		    Console.Error.WriteLine ("Failed to save to temporary file");
		    return;
		}

		// Move the file to the real one
		try {
			File.Copy(tmpFile, this.FileName, true);
			File.Delete(tmpFile);
		} catch (Exception e) {
		    Console.Error.WriteLine ("File replace error: " + e.Message);
		    return;
		}
		}
	}

        public async Task Add (IChannel channel, bool update = false)
        {
            // If we already have this feed, simply return
			if (mChannels.Cast<Channel>().Any(channel.Url.Equals))
                return;

			if(channel.Name == null){
				channel.Name = channel.Url;
			}

			mChannels.Add (channel);
			if (update) {
				bool res = await channel.RefreshAsync();
				if (res)
					MarkAsDirty();
			}

			NotifyAdd(channel);
		}

        public async void Add(ChannelGroup group, IChannel channel)
        {
            group.Add(channel);
			channel.PropertyChanged += ChannelChanged;

			NotifyAdd(new { Group = group, Channel = channel });

			MarkAsDirty();
		}

		void ChannelChanged(object sender, PropertyChangedEventArgs args)
		{
			IChannel channel = (IChannel)sender;
			MarkAsDirty();

			if (PropertyChanged != null)
				PropertyChanged(this, args);
		}

		void DoRemove(IChannel channel)
		{
			/* Try to find out from which list we need to remove. */
			if(mChannels.Contains(channel)){
				mChannels.Remove (channel);
			} else if(Groups.Contains(channel)){
				Groups.Remove(channel);
			} else {
				/* It's not a first-level channel or group. Dig deeper. */
				foreach(ChannelGroup group in Groups){
					if(group.Channels.Contains(channel)){
						group.Channels.Remove(channel);
						break;
					}
				}
			}

			NotifyRemove(channel);
		}

		public void Remove (IChannel channel)
		{
			DoRemove(channel);
			channel.RemoveItems();
			MarkAsDirty();
		}

		public void Move(IChannel src, IChannel dst)
		{
			DoRemove(src);
			if (dst == null)
				Add(src);
			else
				Add((ChannelGroup)dst, src);

			MarkAsDirty();
		}

		public async void RefreshAll()
		{
			var all = mChannels.Cast<IChannel>().Union(Groups.Cast<IChannel>());

			foreach (var chan in all) {
				EmitChannelRefreshStarted(chan);
				await chan.RefreshAsync();
				EmitChannelRefreshFinished(chan);
			}
		}

	private void EmitChannelRefreshStarted (IChannel channel)
	{
	    if (ChannelRefreshStarted != null)
				GLib.Idle.Add(() => {
					ChannelRefreshStarted(channel);
					return false;
				});
	}

		void EmitChannelRefreshFinished(IChannel channel)
		{
			if (ChannelRefreshFinished != null)
				GLib.Idle.Add(() => {
					ChannelRefreshFinished(channel);
					return false;
				});
		}


		void MarkAsDirty()
		{
			if (DelayedWrite != null && !DelayedWrite.IsCompleted)
				return;

			DelayedWrite = Task.Delay(TimeSpan.FromMilliseconds(WRITE_TIMEOUT));
			DelayedWrite.ContinueWith(task => {
				SaveToFile();
				ItemStore.Save();
			});
		}

	private static string GetTempFile (string fileName) 
	{
	    return fileName + ".tmp";
	}
    }
}

